.ROMDMG                    ; DMG game
.NAME "NAME"               ; ROM name
.CARTRIDGETYPE 0           ; ROM only cartridge
.RAMSIZE 0                 ; No external RAM
.EMPTYFILL $00             ; Padding

.LICENSEECODENEW "00"      ; License code from Nintendo, kek

.COMPUTEGBCHECKSUM         ; Tell wla-dx to compute cartridge checksum
.COMPUTEGBCOMPLEMENTCHECK  ; Tell wla-dx to check header validity

.MEMORYMAP
SLOTSIZE $4000
DEFAULTSLOT 0
SLOT 0 $0000
SLOT 1 $4000
.ENDME

.ROMBANKSIZE $4000
.ROMBANKS 2

.BANK 0 SLOT 0

.ORG $0040  ; Vblank interruption
call Vblank
reti

.ORG $0100  ; Entry point
nop
jp start    ; $0150

.ORG $0104  ; Nintendo Logo, act as a cartridge checksum
.DB $CE,$ED,$66,$66,$CC,$0D,$00,$0B,$03,$73,$00,$83,$00,$0C
.DB $00,$0D,$00,$08,$11,$1F,$88,$89,$00,$0E,$DC,$CC,$6E,$E6
.DB $DD,$DD,$D9,$99,$BB,$BB,$67,$63,$6E,$0E,$EC,$CC,$DD,$DC
.DB $99,$9F,$BB,$B9,$33,$3E

.ORG $0150  ; We are right after the cartridge header

;             -- start --
; Clear the background and the OAM
; Load graphics and game initial state
start:
  di              ; disable interrupts
  ld sp,$FFF4     ; SP point to HRAM, as adviced by Nintendo

  xor a           ; a = 0
  ldh ($26), a    ; Mute sound to save battery life

  ; Wait until vblank to clear VRAM
  waitvbl:
    ldh a, ($44)  ; Which line the GPU is refreshing
    cp 144        ; The Gameboy screen have 144 lines
    jr c, waitvbl ; If $FF44 < 144, the GPU is not in vblank, wait more

  xor a           ; a = 0
  ldh ($40), a    ; Shutdown the screen
  ldh ($42), a    ; background scroll x = 0
  ldh ($43), a    ; background scroll y = 0

  ;    -- You should load your graphic data here --      ;

  ;        -- clear_background --
  ; Clear the background with the tile 0
  ld de, 32*32     ; counter. The background is a 32 by 32 tilemap
  ld hl, $9800    ; Point to the first background tile in VRAM

  clear_background:
    xor a         ; a = 0
    ldi (hl), a   ; put 0 at address (HL), and increment (HL)
    dec de        ; Decrement our tile counter

    ; The Z flag is not set when using dec on a 16bit register
    ; We need to manually check if it is equal to zero
    ld a, e
    or d          ; a = e or d. If both equal zero, then de is equal to zero.
    jr nz, clear_background ; If it is not, then jump

  ;   -- clear_oam --
  ; Clear the OAM with 0
  ld b, 40*4         ; counter. The OAM contain 40 sprite which are defined on 4 bytes
  ld hl, $FE00       ; Point to OAM adress

  clear_oam:
    ld (hl), $00     ; set byte to 0 at (HL) address
    inc l            ; There is a hardware bug when in OAM that doesn't let us use ldi, we need to increase l manually
    dec b            ; decrease our counter
    jr nz, clear_oam ; Jump until b is equal to zero


  ; -- You should initialize your game variable here --  ;

  ; load background and sprites palettes
  ld a, %11100100    ; black, dark grey, light grey and white
  ldh ($47), a       ; Set background palette
  ldh ($48), a       ; Set first sprites palette
  ldh ($49), a       ; Set second sprites palette

  ld a, %10010011    ; Screen on, BG on, tiles at $8000
  ldh ($40), a

  ; Enable VBlank interrupt
  ld a, %00010000
  ldh ($41), a
  ld a, %00000001
  ldh ($FF), a

  ei                 ; Activate interruptions

;             -- loop --
; Infinite loop, wait for interruptions
loop :
  jr loop

;            -- Vblank --
; Jump to this label from Vblank interruption
; Update graphics and game state.
Vblank:
  push af
  push hl

  ; -- Stuff -- ;

  pop hl
  pop af
  ret

