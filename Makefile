EXEC = template

all: rom

rom:
	wla-gb -ox main.s main.o
	echo [objects] > linkfile
	echo main.o >> linkfile
	wlalink -vs linkfile $(EXEC).gb

clean:
	rm *.o
	rm linkfile
	rm $(EXEC).gb
