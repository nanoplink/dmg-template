wla-dx DMG template
========================

This repo aim to be a basic project template to produce a DMG ROM in Z80
ASM with wla-dx.

It provides :

* Routines to empty and initialise the VRAM 
* A small structure in the source code
* A makefile to build the ROM
* Commentaries to understand what's going on

All of that packed under a [cute open license][lil-license]. Grab and share ! 

Rationale
------------------------

This template was made to ease the bootstrap of a project. Setup properly a ROM
need some tedious, repetitive work that doesn't really change. 

It's quite rough and isn't meant to be full-fledged. It was quickly made for
myself and I thought it could be a good thing to share it with others.

It can also be a good start point to understand how the DMG works. However please note that
even if i'm trying my best to explain everything in the source file, you should
try to understand it fully.
That means that you should be able to reproduce it. It's for your own good :)

Requirements
------------------------

* You need to know Z80 ASM
* You need to know how the hardware works (you can find the documentation [here][pandoc])
* An emulator or a real DMG
* [WLA DX][wla-dx] 9.4+
* A make tool

Usage
------------------------

1. clone or download the repo.
2. Add some code in main.s.
3. `make rom` to assemble the ROM
4. ?????
5. Profit!
6. `make clean` to remove staging files

HELP I DON'T UNDERSTAND ANYTHING
------------------------

I don't have any ressources to help you atm. but if :

1. You know what a register is
2. You know what a pointer is

Then you'll be able to understand what's going on.

You can find a dump of a conversation [here][help-asm] where i explain
step by step what the themplate does.

You can also find me or others competents persons on the [PSG Cabal][psg-cabal]
Discord server if you need to chat!

Contributing
------------------------

I don't have a contributing guide yet but if you want to help don't hesitate to
open an issue or a pull request!

License
------------------------

❤❤❤ made in 2016 by nanoplink. Copying is an act of love. Please copy. ❤❤❤

This work is licensed under The Lil License v1. See the [LICENSE](LICENSE) file
or the [license's website][lil-license].


[wla-dx]:      http://www.villehelin.com/wla.html
[pandoc]:      http://bgb.bircd.org/pandocs.htm
[help-asm]:    https://www.zerobin.net/?a81518e1f7b3c3a7#7H+CVxRKpqC+gbgdGdkJ0Hgz0vQBxsd629gOyzRolcQ=
[psg-cabal]:   https://discord.io/psgcabal
[lil-license]: http://lillicense.org/v1.html
